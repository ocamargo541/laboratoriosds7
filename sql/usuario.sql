-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-08-2022 a las 01:52:05
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laboratoriouno`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `cedula` varchar(25) NOT NULL,
  `direccion` varchar(25) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `sexo` varchar(25) DEFAULT NULL,
  `institucion` varchar(25) NOT NULL,
  `especialidad` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellido`, `email`, `cedula`, `direccion`, `telefono`, `fecha`, `sexo`, `institucion`, `especialidad`) VALUES
(1, '', '', '', '', '', '', NULL, '', '', ''),
(2, 'goku', 'martinez', 'goku@gmail.com', '277374', 'petare', '090909', NULL, '', 'Privada', 'Tecnología'),
(3, 'goku', 'martinez', 'goku@gmail.com', '277374', 'petare', '090909', NULL, '', 'Privada', 'Tecnología'),
(4, 'Orlando', 'Camargo', 'orlando.camargo@utp.ac.pa', '8-982-605', 'Las Cumbres Casa Real  ca', '6532-2339', NULL, 'masculino', 'Privada', 'Tecnología'),
(5, 'Juan', 'Pepe', 'juan@juanito.com', '8-982-605', 'micasa', '21223-3443', NULL, 'masculino', 'Gubernamental', 'Ingeniería'),
(6, 'Omar', 'Camargo', 'omar@gmail.com', '8-982-605', 'una casa fea', '65232-2339', NULL, 'masculino', 'Gubernamental', 'Educación'),
(7, '', '', '', '', '', '', NULL, '', 'Gubernamental', 'Ingeniería'),
(8, '', '', '', '', '', '', NULL, '', 'Gubernamental', 'Ingeniería'),
(9, 'sdfdsfds', 'dsfsdf', 'dssdfsd', 'dsfsfsd', '', 'sdfsdf', NULL, 'femenino', 'Gubernamental', 'Ingeniería'),
(10, '', '', '', '', '', '', NULL, '', 'Gubernamental', 'Ingeniería');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
